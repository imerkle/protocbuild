module protocbuild

go 1.15

require (
	github.com/go-git/go-git/v5 v5.3.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.0
	github.com/google/go-github/v34 v34.0.0
	github.com/otiai10/copy v1.6.0
	golang.org/x/oauth2 v0.0.0-20210402161424-2e8d93401602
	google.golang.org/protobuf v1.26.0 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
